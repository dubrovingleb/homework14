﻿
#include <iostream>
#include <string>

int main()
{
    std::string S = "012345";
    std::cout << "String:" << S << "\n";
    std::cout << "length:" << S.length() << "\n";
    std::cout << "first index:" << S[0] << "\n";
    std::cout << "last index:" << S[S.size() - 1] << "\n";
}


